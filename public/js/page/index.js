var module = angular.module('main', ['angularBetterPlaceholder', 'ngMockE2E', 'ngRoute', 'ngAnimate'])
			.config(['$provide', '$routeProvider', '$locationProvider', function ($provide, $routeProvider, $locationProvider) {
				$provide.decorator('$httpBackend', function ($delegate) {
					var proxy = function (method, url, data, callback, headers) {
						var interceptor = function () {
							var _this = this,
								_arguments = arguments;
							setTimeout(function () {
								callback.apply(_this, _arguments)
							}, 700);
						};
						return $delegate.call(this, method, url, data, interceptor, headers);
					};
					for(var key in $delegate) {
						proxy[key] = $delegate[key];
					}
					return proxy;
				});

				$routeProvider
					.when('/login', {
						templateUrl: 'app/user.html',
						controller: 'UserCtrl'
					})
					.when('/report', {
						templateUrl: 'app/report.html',
						controller: 'ReportCtrl',
						resolve: {
							reportdata: ['$http', function ($http) {
								return $http.get('data/report.json').then(function (data) {
									return data.data;
								});
							}]
						}
					})
					.otherwise({
						redirectTo: '/login'
					});
				$locationProvider.html5Mode(true);
			}]).run(['$httpBackend', function ($httpBackend) {
				$httpBackend.whenPOST('/login').respond(function(method, url, data) {
					var details = angular.fromJson(data);
					if (details.email && 
						details.email === "yhwwen@126.com" &&
						details.password &&
						details.password === "123456") {
						return [200, {loggedIn: true, userid: 'testid'}, {}];
					} else {
						return [200, {loggedIn: false}, {}];
					}
				});
				$httpBackend.whenGET(/.*/i).passThrough();
			}]);

module.factory('UserData', function () {
	var data  = {
		email: "",
		password: ""
	};

	return data;
});

module.controller('mainCtrl', ['$scope', function ($scope) {

}]);

module.controller('UserCtrl', ['$scope', '$http', '$location', 'UserData', function ($scope, $http, $location, UserData) {
	$scope.data = UserData;
	$scope.pageClass = "login";
	$scope.loading = false;
	$scope.postResult = 0;
	$scope.submit = function () {
		$scope.loading = true;
		$http.post('/login', $scope.data).success(function (data) {
			$scope.loading = false;
			if (data.loggedIn) {
				$scope.postResult = 1;
				$location.url("/report");
			} else {
				$scope.postResult = 2;
			}
		});
	};
}]);

module.controller('ReportCtrl', ['$scope', '$http', '$filter', 'reportdata', 'UserData', function ($scope, $http, $filter, reportdata, UserData) {
	$scope.data = reportdata;
	$scope.pageClass = "report";
	$scope.userdata = UserData;
	$scope.predicate = 'name';
	$scope.reverse = false;

	$scope.change = function (predicateName) {
		console.log(predicateName)
		if (predicateName === $scope.predicate) {
			$scope.reverse = !$scope.reverse;
		} else {
			$scope.predicate = predicateName;
			$scope.reverse = false;
		}
	}

	$scope.sendEmails = function () {
		var emails = [],
			items = $filter('filter')($scope.data, $scope.filter);
		for (var i = 0, length = items.length; i < length; i += 1) {
			console.log(items[i].selected);
			if (items[i].selected) {
				emails.push(items[i].email);
			}
		}

		alert('Email sent to ' + emails.join(', \n'))
	}

	$scope.deleteRow = function (row) {
		for (i = 0, length = $scope.data.length; i < length; i += 1) {
			if (row === $scope.data[i]) {
				break;	
			}
		}

		$scope.data.splice(i, 1);
	}
}]);